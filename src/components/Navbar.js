import React from 'react'
import Navbar from 'react-bootstrap/Navbar'
import {Nav, Button, Form, FormControl} from 'react-bootstrap'
import axios from 'axios'
function getCurrentData(setCurrentData,search) {
    axios
      .get(
        `https://api.giphy.com/v1/gifs/search?api_key=${process.env.REACT_APP_GIPHY}&q=${search}&limit=25&offset=0&rating=g&lang=en`
      )
      .then((response) =>
        setCurrentData({
          response,
        })
      )
      .catch((error) => {
        console.log(error);
      });
  }

export default function NavBarcomponent(props){
    return (
        <>
  <Navbar bg="dark" variant="dark">
    <Navbar.Brand href="#home">GifGrid</Navbar.Brand>
    <Form inline>
        <input onChange={event => props.setState(event.target.value)}></input>
      <Button variant="outline-info" onClick={()=>getCurrentData(props.setData,props.searchquery)}>Search</Button>
    </Form>
  </Navbar>
  </>
    )
}
