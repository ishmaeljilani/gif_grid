import logo from "./logo.svg";
import styles from "./App.module.css";
import responsedefault from "./reponse.json";
import GifPlayer from "react-gif-player";
import Navbar from './components/Navbar'
import React, {useState} from 'react'
import axios from 'axios'

function App() {
  const [search, setSearch] = useState('')
  const [currentData, setCurrentData] = useState(responsedefault)
  return (
    <>
    <Navbar setState={setSearch} searchquery={search} setData={setCurrentData}></Navbar>
    <div className={styles.photos}>
      {currentData.response.data.data.map((e) => (
         <GifPlayer
           gif={e.images.original.url}
           still={e.images.original_still.url}
         />
       ))}
    </div>
    </>
  );
}

export default App;
